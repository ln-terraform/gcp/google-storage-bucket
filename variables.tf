variable "project" {
  type = string
}

variable "bucket" {
  type = string
}

variable "storage_class" {
  type    = string
  default = "STANDARD"
}

variable "location" {
  type    = string
  default = "ASIA-SOUTHEAST2"
}

variable "versioning" {
  type    = bool
  default = true
}

variable "cors" {
  type = object({
    origin          = list(string)
    method          = list(string)
    response_header = list(string)
    max_age_seconds = number
  })

  default = {
    origin          = []
    method          = []
    response_header = []
    max_age_seconds = 3600
  }
}

variable "google_accounts_permissions" {
  type = list(object({
    google_account = string
    roles          = list(string)
  }))
  default = []
}

variable "service_accounts_permissions" {
  type = list(object({
    service_account = string
    roles           = list(string)
  }))
  default = []
}

variable "google_groups_permissions" {
  type = list(object({
    google_group = string
    roles        = list(string)
  }))
  default = []
}

variable "gsuite_domains_permissions" {
  type = list(object({
    gsuite_domain = string
    roles         = list(string)
  }))
  default = []
}

variable "allow_all_users" {
  type    = bool
  default = false
}

variable "allow_all_authenticated_users" {
  type    = bool
  default = false
}

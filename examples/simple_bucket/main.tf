module "google_cloud_storage" {
  source = "../.."

  project                       = "gcloud-309315"
  bucket                        = "gcloud-309315-test-bucket"
  storage_class                 = "STANDARD"
  location                      = "ASIA-SOUTHEAST2"
  allow_all_users               = true

  google_accounts_permissions = [
    {
      google_account = "linhheo.cloud5@gmail.com",
      roles          = ["roles/storage.admin"]
    },
  ]

  cors = {
    origin          = ["*"]
    method          = ["*"]
    response_header = ["*"]
    max_age_seconds = 3600
  }
}
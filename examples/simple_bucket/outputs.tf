output "bucket_id" {
  value = module.google_cloud_storage.bucket_id
}

output "bucket_selflink" {
  value = module.google_cloud_storage.bucket_selflink
}

output "bucket_url" {
  value = module.google_cloud_storage.bucket_url
}
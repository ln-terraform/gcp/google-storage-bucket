locals {
  roles = setunion(
    flatten([for ga in var.google_accounts_permissions : ga.roles]),
    flatten([for sa in var.service_accounts_permissions : sa.roles]),
    flatten([for gg in var.google_groups_permissions : gg.roles]),
    flatten([for gd in var.gsuite_domains_permissions : gd.roles]),
  )

  google_accounts  = toset([for ga in var.google_accounts_permissions : "user:${ga.google_account}"])
  service_accounts = toset([for sa in var.service_accounts_permissions : "serviceAccount:${sa.service_account}"])
  google_groups    = toset([for gg in var.google_groups_permissions : "group:${gg.google_group}"])
  gsuite_domains   = toset([for gd in var.gsuite_domains_permissions : "domain:${gd.gsuite_domain}"])
}

resource "google_storage_bucket" "bucket" {
  name                        = var.bucket
  project                     = var.project
  storage_class               = var.storage_class
  location                    = var.location
  force_destroy               = true
  uniform_bucket_level_access = true
  cors {
    origin          = var.cors.origin
    method          = var.cors.method
    response_header = var.cors.response_header
    max_age_seconds = var.cors.max_age_seconds
  }

  versioning {
    enabled = var.versioning
  }
}

resource "google_storage_bucket_iam_binding" "members_binding" {
  for_each = local.roles

  bucket = google_storage_bucket.bucket.name
  role   = each.key
  members = setunion(
    [for ga in var.google_accounts_permissions : "user:${ga.google_account}" if contains(ga.roles, each.key)],
    [for sa in var.service_accounts_permissions : "serviceAccount:${sa.service_account}" if contains(sa.roles, each.key)],
    [for gg in var.google_groups_permissions : "group:${gg.google_group}" if contains(gg.roles, each.key)],
    [for gd in var.gsuite_domains_permissions : "domain:${gd.gsuite_domain}" if contains(gd.roles, each.key)]
  )

  #condition {
  #  title       = "expires_after_2019_12_31"
  #  description = "Expiring at midnight of 2019-12-31"
  #  expression  = "request.time < timestamp(\"2020-01-01T00:00:00Z\")"
  #}

}

resource "google_storage_bucket_iam_binding" "allow_all_users" {
  count = var.allow_all_users ? 1 : 0

  bucket  = google_storage_bucket.bucket.name
  role    = "roles/storage.objectViewer"
  members = ["allUsers"]
}

resource "google_storage_bucket_iam_binding" "allow_all_authenticated_users" {
  count = var.allow_all_authenticated_users ? 1 : 0

  bucket  = google_storage_bucket.bucket.name
  role    = "roles/storage.objectViewer"
  members = ["allAuthenticatedUsers"]
}
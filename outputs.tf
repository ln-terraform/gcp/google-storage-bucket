output "bucket_id" {
  value = google_storage_bucket.bucket.id
}

output "bucket_selflink" {
  value = google_storage_bucket.bucket.self_link
}

output "bucket_url" {
  value = google_storage_bucket.bucket.url
}